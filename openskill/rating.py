from dataclasses import dataclass, field
from typing import Dict, Optional, Any

from .constants import DEFAULT_MU


@dataclass
class Rating:

    z: int = 3

    mu: float = DEFAULT_MU

    sigma: float = field(default=None)  # type: ignore

    metadata: Dict[str, Any] = field(default_factory=dict)

    def __post_init__(self):
        if not self.sigma:
            self.sigma = self.mu / self.z

    def copy(self, **kwargs) -> "Rating":
        copy_data = {
            "z": self.z,
            "mu": self.mu,
            "sigma": self.sigma,
            "metadata": self.metadata,
        }
        copy_data.update(kwargs)

        return Rating(**copy_data)

    def get_ordinal(self) -> float:
        return self.mu - self.z * self.sigma
