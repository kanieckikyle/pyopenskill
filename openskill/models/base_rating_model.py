from abc import ABCMeta, abstractmethod
from typing import List
from math import sqrt

from ..constants import DEFAULT_MU
from ..rating import Rating
from ..team_rating import TeamRating


class BaseRatingModel(metaclass=ABCMeta):

    z: int

    mu: float

    sigma: float

    epsilon: float

    beta: float

    beta_squared: float

    two_beta_squared: float

    def __init__(self, z=3, mu=DEFAULT_MU, sigma=None, epsilon=0.0001):
        self.z = z
        self.mu = mu
        self.sigma = sigma or self.mu / self.z

        self.epsilon = epsilon

        self.beta = self.sigma / 2
        self.beta_squared = self.beta ** 2
        self.two_beta_squared = 2 * self.beta_squared

    @abstractmethod
    def rate(self, game: List[TeamRating]):
        pass

    def gamma(self, c_value: float, k_value: int, team_rating: TeamRating) -> float:
        return sqrt(team_rating.sigma_squared) / c_value
