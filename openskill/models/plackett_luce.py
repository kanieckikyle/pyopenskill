from math import exp, sqrt
from typing import List

from ..rating import Rating
from ..team_rating import TeamRating
from .base_rating_model import BaseRatingModel


class PlackettLuce(BaseRatingModel):
    def rate(self, game: List[TeamRating]) -> List[TeamRating]:
        self.team_ratings = game

        for i in range(len(self.team_ratings)):
            self.team_ratings[i].rank = i

        c = self.util_c(self.team_ratings)
        sum_q = self.util_sum_q(self.team_ratings, c)
        a = self.util_a(self.team_ratings)

        new_ratings = []
        for i_index, i_rating in enumerate(self.team_ratings):
            i_mu_over_c = exp(i_rating.mu / c)

            omega_sum = 0.0
            delta_sum = 0.0

            q_ratings = [
                rating for rating in self.team_ratings if rating.rank <= i_rating.rank
            ]
            for q in range(len(q_ratings)):
                quotient = i_mu_over_c / sum_q[q]

                omega_sum += (1 - quotient if i_index == q else -quotient) / a[q]
                delta_sum += (quotient * (1 - quotient)) / a[q]

            i_gamma = self.gamma(c, len(self.team_ratings), i_rating)
            i_omega = omega_sum * (i_rating.sigma_squared / c)
            i_delta = i_gamma * delta_sum * (i_rating.sigma_squared / c ** 2)

            new_ratings.append(
                TeamRating(
                    [
                        member_rating.copy(
                            mu=member_rating.mu
                            + (member_rating.sigma ** 2 / i_rating.sigma_squared)
                            * i_omega,
                            sigma=member_rating.sigma
                            * sqrt(
                                max(
                                    1
                                    - (
                                        member_rating.sigma ** 2
                                        / i_rating.sigma_squared
                                    )
                                    * i_delta,
                                    self.epsilon,
                                )
                            ),
                        )
                        for member_rating in i_rating.member_ratings
                    ]
                )
            )

        return new_ratings

    def util_c(self, team_ratings: List[TeamRating]) -> float:
        return sqrt(
            sum(
                team_rating.sigma_squared + self.beta_squared
                for team_rating in team_ratings
            )
        )

    def util_sum_q(self, team_ratings: List[TeamRating], c_value) -> List[float]:
        temp = []
        for q_rating in team_ratings:
            temp.append(
                sum(
                    [
                        exp(i_rating.mu / c_value)
                        for i_rating in team_ratings
                        if i_rating.rank >= q_rating.rank
                    ]
                )
            )

        return temp

    def util_a(self, team_ratings: List[TeamRating]) -> List[int]:
        a_values = []
        for i_rating in team_ratings:
            a_values.append(
                len(
                    list(
                        filter(
                            lambda q_rating: i_rating.rank == q_rating.rank,
                            team_ratings,
                        )
                    )
                )
            )

        return a_values
