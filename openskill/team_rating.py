from .rating import Rating

from typing import List


class TeamRating:

    mu: float

    sigma_squared: float

    member_ratings: List[Rating]

    def __init__(self, member_ratings: List[Rating], rank: int = 0):
        self.member_ratings = member_ratings

        self.mu = sum(rating.mu for rating in member_ratings)

        self.sigma_squared = sum(
            rating.sigma * rating.sigma for rating in member_ratings
        )

        self.rank = rank

    def __str__(self):
        display_str = "Team Rating:\n"
        for member in self.member_ratings:
            display_str += f"\t{member.mu, member.sigma}\n"
        return display_str

    def __repr__(self):
        return str(self)
